def calculate_media(numbers):
    total = 0
    elems_total = len(numbers)
    for num in numbers:
        total+=num
    return total/elems_total


def calculate_deviation(media,numbers):
    deviation_sum = 0
    for num in numbers:
        difference = media - num
        num_desv = difference * difference
        deviation_sum += num_desv
    return deviation_sum / len(numbers)


def calculate_float_part_of_sqrt(num,decimal_num):
    diff = .1
    float_num = float(decimal_num)
    while True:
        temp_f_num = float_num + diff
        temp_sqrt = temp_f_num * temp_f_num

        if round(temp_sqrt,8) == round(num,8):
            return round(temp_f_num, 8)
        elif temp_sqrt < num:
            float_num = temp_f_num

        else:
            diff = diff * .1


def calculate_sqrt(num):
    bottom = 0
    top = int(num / 2)
    while True:
        mid = int((top + bottom) / 2)

        temp_sqrt = mid * mid

        if temp_sqrt == num:
            return mid
        elif temp_sqrt<num:
            if bottom == mid:
                return calculate_float_part_of_sqrt(num,mid)
            else:
                bottom = mid
        else:
            top = mid


def calculate_standard_deviation(dataset):
    media = calculate_media(dataset)
    deviation = calculate_deviation(media, dataset)
    standard_deviation = calculate_sqrt(deviation)

    return standard_deviation


f = open("dataset1.txt", "r")
f2 = open("dataset2.txt", "r")

dataset1 = []
dataset2 = []

for line in f:
    dataset1.append(int(line))

for line in f2:
    dataset2.append(int(line))

print("Standard Deviation of dataset1:", calculate_standard_deviation(dataset1))
print("Standard Deviation of dataset2:", calculate_standard_deviation(dataset2))

