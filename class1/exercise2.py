import random
try:
    num_guessed = False
    games_count = 0
    numbers_guessed = 0

    while True:
        guess_num = random.randint(1, 9)
        user_input = input("Can you guess the number I am thinking (from 1 to 9)? ")

        if user_input.lower() == 'exit':
            break
        else:
            games_count += 1
            user_guess_num = int(user_input)

            if user_guess_num == guess_num:
                numbers_guessed += 1
                print("You guessed the number")

            if user_guess_num<guess_num:
                print(f"You guessed too low")
            elif user_guess_num>guess_num:
                print(f"You guessed too high")

    print(f"You guessed {numbers_guessed} time(s) of {games_count}")
except ValueError:
    print("Error: A number or 'exit' were expected to be provided")