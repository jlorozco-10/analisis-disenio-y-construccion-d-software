def remove_duplicates_using_loop(lista):
    list_without_duplicates = []
    for item in lista:
        if item not in list_without_duplicates:
            list_without_duplicates.append(item)
    return list_without_duplicates


def remove_duplicates_using_sets(lista):
    return list(set(lista))


print(remove_duplicates_using_loop([6,5,4,3,4,2,4,7,8,9,10,3,4,5,7,5,4,3,2,44,53,6,77,5,4,2,3,1,3,4,5,6,7,8]))

print(remove_duplicates_using_sets([6,5,4,3,4,2,4,7,8,9,10,3,4,5,7,5,4,3,2,44,53,6,77,5,4,2,3,1,3,4,5,6,7,8]))
