def convert_from_decimal_to_romans(num):
    roman_number =""

    if 4000 > num > 0:
        miles = num // 1000
        if miles > 0:
            if miles < 4:
                roman_number += "M"*miles
                num = num - 1000 * miles

        centenas = num // 100
        if centenas > 0:
            if centenas < 4:
                roman_number += "C" * centenas
            elif centenas == 4:
                roman_number += "CD"
            elif centenas < 9:
                roman_number += "D"+ ("C"*(centenas-5))
            else:
                roman_number += "CM"
            num = num - 100 * centenas

        decenas = num // 10
        if decenas > 0:
            if decenas < 4:
                roman_number += "X" * decenas
            elif decenas == 4:
                roman_number += "XL"
            elif decenas < 9:
                roman_number += "L"+ ("X"*(decenas-5))
            else:
                roman_number += "XC"

            num = num - 10 * decenas

        if num > 0:
            if num < 4:
                roman_number += "I" * num
            elif num == 4:
                roman_number += "IV"
            elif num < 9:
                roman_number += "V" + ("I" * (num - 5))
            else:
                roman_number += "IX"
    else:
        return "Invlid Number"

    return roman_number


decimal_num = 2583
print(f"{decimal_num}: "+ convert_from_decimal_to_romans(decimal_num))