def calculate_mean(numbers):
    try:
        total = 0
        elems_total = len(numbers)
        for num in numbers:
            total+=num
        return total/elems_total

    except TypeError:
        print("Error: Please provide a list of elements")


def __calculate_deviation(media,numbers):
    deviation_sum = 0
    for num in numbers:
        difference = media - num
        num_desv = difference * difference
        deviation_sum += num_desv
    return deviation_sum / len(numbers)


def __calculate_float_part_of_sqrt(num,decimal_num):
    diff = .1
    float_num = float(decimal_num)
    while True:
        temp_f_num = float_num + diff
        temp_sqrt = temp_f_num * temp_f_num

        if round(temp_sqrt,8) == round(num,8):
            return round(temp_f_num, 8)
        elif temp_sqrt < num:
            float_num = temp_f_num

        else:
            diff = diff * .1


def __calculate_sqrt(num):
    bottom = 0
    top = int(num / 2)
    while True:
        mid = int((top + bottom) / 2)

        temp_sqrt = mid * mid

        if temp_sqrt == num:
            return mid
        elif temp_sqrt<num:
            if bottom == mid:
                return __calculate_float_part_of_sqrt(num,mid)
            else:
                bottom = mid
        else:
            top = mid


def calculate_standard_deviation(dataset):
    try:
        media = calculate_mean(dataset)
        deviation = __calculate_deviation(media, dataset)
        standard_deviation = __calculate_sqrt(deviation)
        return standard_deviation
    except TypeError:
        print("Error: Please provide a list of elements")


def calculate_median(dataset):
    try:
        numbers_ordered = sorted(dataset)
        mid = int(len(dataset) / 2)
        if len(dataset) % 2 == 0:
            return (numbers_ordered[mid] + numbers_ordered[mid-1]) / 2
        else:
            return numbers_ordered[mid]
    except TypeError:
        print("Error: Please provide a list of elements")


def calculate_n_quartil(dataset,n):
    dataset_ordered = sorted(dataset)
    is_even = True if len(dataset) % 2 == 0 else False

    if n == 1:
        if is_even:
                first_quarter = int(len(dataset) / 4)
                return dataset_ordered[first_quarter] - (0.25 * (dataset_ordered[first_quarter] - dataset_ordered[first_quarter-1]))
        else:
            first_quartil_pos = len(dataset) // 4
            return dataset_ordered[first_quartil_pos]

    elif n == 2:
        return calculate_median(dataset)

    elif n == 3:
        if is_even:
            third_quarter = int(len(dataset) / 4 * 3)

            return dataset_ordered[third_quarter] - (0.75 * (dataset_ordered[third_quarter] - dataset_ordered[third_quarter - 1]))
        else:
            third_quartil_pos = len(dataset)//2 + round(len(dataset) / 4)
            return dataset_ordered[third_quartil_pos]


def calculate_percentil(dataset,percent):

    x = percent * len(dataset) / 100

    diff = round(x - (percent * len(dataset) // 100),1)

    if diff != 0.0:
        print(diff)

    print(x,diff)



