class NotANumberError(ValueError):
    pass


class FileNameIsNotCorrectError(FileNotFoundError):
    pass
