"""
Class that will read a CSV file (unsorted values) and will return a sorted CSV file.
"""
import csv
import cumstom_exceptions


class SortLists:
    def __init__(self):
        self.list_of_nums = []
        self.reference_list = []
        self.num_of_elems_per_line = 5

    def set_input_data(self,file_name_path):
        try:
            self.list_of_nums = []
            with open(file_name_path, "r") as input_file:
                reader = csv.reader(input_file)
                for row in reader:
                    for elem in row:
                        self.list_of_nums.append(int(elem))
                return self.list_of_nums
        except ValueError:
            # raise NotANumberError("Value read is not a number")
            print("Value read is not a number")
            return -2
        except FileNotFoundError:
            # raise FileNameIsNotCorrectError("Error while trying to open the file")
            print("Error while trying to open the file")
            return -3

    def set_output_data(self, file_name_path):
        try:
            with open(file_name_path, "w", newline='') as output_file:
                writer = csv.writer(output_file)
                length_of_list = len(self.list_of_nums)
                # print(length_of_list)
                bottom_index = 0
                num_of_elems_per_line = self.num_of_elems_per_line
                starting_index = self.num_of_elems_per_line
                i = 0
                for i in range(starting_index, length_of_list + 1, num_of_elems_per_line):
                    writer.writerow(self.list_of_nums[bottom_index: i])
                    bottom_index = i
                else:
                    # To add the remaining elements when length_of_list is not divisible
                    # equally by length_of_elements_per_line
                    if i < length_of_list:
                        writer.writerow(self.list_of_nums[i:])
                return 1
        except FileNotFoundError:
            # raise FileNameIsNotCorrectError("Error while trying to open the file")
            print("Error while trying to open the file")
            return -3

    def execute_merge_sort(self):
        l_index = 0
        r_index = len(self.list_of_nums)-1
        self.__merge_sort__(l_index, r_index)

        return self.list_of_nums

    # Method used to directly assign the list_of_numbers for testing purpose
    def set_list_value(self,u_list):
        self.list_of_nums = u_list

    def __merge_sort__(self, l_index, r_index):
        if l_index < r_index:
            halve_index = l_index + (r_index - l_index) // 2
            self.__merge_sort__(l_index, halve_index)
            self.__merge_sort__(halve_index+1, r_index)
            self.__merge__(l_index,halve_index,r_index)

    def __merge__(self, l_list, halve_index, r_list):
        #print(f"halve: {halve_index}")
        #print(f"left index: {l_list}")
        #print(f"right index: {r_list}")
        self.reference_list = self.list_of_nums[:]
        first_idx = l_list
        second_idx = halve_index + 1

        # first_num = self.list_of_nums[first_idx]
        # second_num = self.list_of_nums[second_idx]

        for i in range(l_list, r_list+1):

            if first_idx > halve_index:
                break

            elif second_idx > r_list:
                self.list_of_nums[i] = self.reference_list[first_idx]
                first_idx += 1

            else:
                first_num = self.reference_list[first_idx]
                second_num = self.reference_list[second_idx]

                if first_num > second_num:
                    self.list_of_nums[i] = second_num
                    second_idx += 1
                else:
                    self.list_of_nums[i] = first_num
                    first_idx += 1

        # print(f" arr: {self.list_of_nums[l_list:r_list+1]}")


# srt_list = SortLists()
#
# srt_list.set_input_data("nums.csv")
# srt_list.execute_merge_sort()
# srt_list.set_output_data("nums_ordered.csv")
#
