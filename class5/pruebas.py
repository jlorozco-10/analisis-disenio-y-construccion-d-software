import csv
import random

# arr = [2,5,6,7,8]

# arr[1:3] = arr[3:5]
# print(arr)

# for i in range(1,101):
#     print(f"{i},", end="")


def generate_csv_with_n_nums():
    for i in range(1,2):
        unordered_list = []
        ordered_list = []

        n = random.randint(100000,900000)
        increment = random.randint(5,16)
        for num in range(1, 1000000):
            ordered_list.append(num)

        unordered_list = mix_list_randomly(ordered_list)
        file_name = f"data{i}.csv"

        print(f"{file_name}: {n}")
        with open(file_name,"w") as file:
            writer = csv.writer(file)
            writer.writerow(ordered_list)
            writer.writerow(unordered_list)


def mix_list_randomly(u_list):
    new_list = u_list[:]
    last_valid_index = len(new_list) - 1
    max_num_of_mix =  last_valid_index // 2

    for i in range(0,max_num_of_mix):
        first_idx = random.randint(0,last_valid_index)
        second_idx = random.randint(0,last_valid_index)

        aux = new_list[first_idx]
        new_list[first_idx] = new_list[second_idx]
        new_list[second_idx] = aux

    return new_list


generate_csv_with_n_nums()