import unittest
from contacts import Person
from contacts import Contacts


class TestContacts(unittest.TestCase):

    def test_add_new_contact(self):
        p = Person("1", "1@test.com", 28, "Mexico")
        c = Contacts()
        self.assertEqual(c.add_contact(p), 1)

    def test_add_new_contact_with_double_validation_getting_all_contacst_after_addition(self):
        p = Person("1", "1@test.com", "28", "Mexico")
        expected_list = [["1", "1@test.com", "28", "Mexico"]]
        c = Contacts()
        self.assertEqual(c.add_contact(p), 1)
        list = c.print_all_contacts()

        self.assertListEqual(list,expected_list)

    # need to check if the assert should be taking in count all columns
    # or just name and email as the find method just require name and email
    def test_find_a_contact_just_one_coincidence(self):
        new_contact = Person("2", "2@test.com", "28", "Mexico")
        find_contact = ['2', '2@test.com', '28', 'Mexico']
        c = Contacts()
        c.add_contact(new_contact)
        finds = c.find_a_contact(find_contact[0], find_contact[1])

        self.assertGreater(len(finds), 0, "Element not found")
        for elem in finds:
            self.assertListEqual(elem, find_contact)

    def test_find_a_contact_multiple_occurrences(self):
        find_contact = ['2', '2@test.com', '28', 'Mexico']
        c = Contacts()
        c.add_contact(Person("2", "2@test.com", "28", "Mexico"))
        c.add_contact(Person("5", "5@test.com", "28", "Mexico"))
        c.add_contact(Person("4", "4@test.com", "28", "Mexico"))
        c.add_contact(Person("2", "2@test.com", "28", "Mexico"))
        finds = c.find_a_contact(find_contact[0], find_contact[1])

        self.assertGreater(len(finds), 0, "Element not found")
        for elem in finds:
            self.assertListEqual(elem, find_contact)

    def test_find_a_contact_no_coincidence(self):
        new_contact = Person("2", "2@test.com", "28", "Mexico")
        find_contact = ['1', '1@test.com', '28', 'Mexico']
        c = Contacts()
        c.add_contact(new_contact)
        finds = c.find_a_contact(find_contact[0], find_contact[1])

        self.assertEqual(len(finds), 0)

    def test_delete_a_contact_just_one_coincidence(self):
        new_contact = Person("6", "6@test.com", "28", "Mexico")
        delete_contact = ['6', '6@test.com', '28', 'Mexico']
        c = Contacts()
        c.add_contact(new_contact)
        finds = c.delete_contact(delete_contact[0], delete_contact[1])

        self.assertGreater(len(finds), 0, "Element not Fount")
        for elem in finds:
            self.assertListEqual(elem, delete_contact)

    # check if the correct hay to evaluate a list of list is thru iteration
    # or if I need to manually create the list of list expected
    # and then evaluate the lists just once
    def test_delete_a_contact_multiple_occurrences(self):
        delete_contact = ['12', '12@test.com', '28', 'Mexico']
        c = Contacts()
        c.add_contact(Person("12", "12@test.com", "28", "Mexico"))
        c.add_contact(Person("5", "5@test.com", "28", "Mexico"))
        c.add_contact(Person("4", "4@test.com", "28", "Mexico"))
        c.add_contact(Person("12", "12@test.com", "28", "Mexico"))
        deletes = c.delete_contact(delete_contact[0], delete_contact[1])

        self.assertGreater(len(deletes), 0, "Element not found")
        for elem in deletes:
            self.assertListEqual(elem, delete_contact)

    def test_delete_a_contact_no_coincidence(self):
        new_contact = Person("6", "6@test.com", "28", "Mexico")
        delete_contact = ['5', '5@test.com', '28', 'Mexico']
        c = Contacts()
        c.add_contact(new_contact)
        finds = c.delete_contact(delete_contact[0], delete_contact[1])
        self.assertEqual(len(finds), 0)

    def test_print_all_contacts(self):
        c = Contacts()
        c.add_contact(Person("12", "12@test.com", "28", "Mexico"))
        c.add_contact(Person("5", "5@test.com", "28", "Mexico"))
        all_contacts = c.print_all_contacts()
        expected_list = [["12", "12@test.com", "28", "Mexico"],["5", "5@test.com", "28", "Mexico"]]

        self.assertListEqual(all_contacts,expected_list)


if __name__ == "__main__":
    unittest.main()
