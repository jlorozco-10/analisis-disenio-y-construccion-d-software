import csv


class Person:
    def __init__(self, name,email,age,country):
        self.name = name
        self.email = email
        self.age = age
        self.country = country


class Contacts:
    def __init__(self):
        with open("contacts.csv", "w", newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['NAME', 'EMAIL', 'AGE', 'COUNTRY'])

    def add_contact(self, person):
        with open("contacts.csv", "a",newline='') as file:
            writer = csv.writer(file)
            writer.writerow([person.name, person.email,person.age,person.country])
            return 1

    def delete_contact(self,name,email):
        new_list = []
        records_deleted = []
        with open("contacts.csv") as file:
            reader = csv.reader(file)

            for row in reader:
                if row[0] != name and row[1] != email:
                    new_list.append(row)
                else:
                  records_deleted.append(row)

        with open("contacts.csv", "w", newline='') as file:
            writer = csv.writer(file)
            writer.writerows(new_list)

        return records_deleted

    def find_a_contact(self,name,email):
        with open("contacts.csv","r") as file:
            reader = csv.reader(file)
            finds = [row for row in reader if row[0] == name and row[1] == email]

            return finds

    def print_all_contacts(self):
        with open("contacts.csv") as file:
            reader = csv.reader(file)
            return list(reader)[1:]