import math
import unittest


class TestMathCeilFunction(unittest.TestCase):

    def test_ceil_method_positive_number_with_decimal_greater_than_point_5(self):
        positive_num = 11
        self.assertEqual(math.ceil(10.8), positive_num)

    def test_ceil_method_positive_number_with_decimal_equals_to_point_5(self):
        positive_num = 15
        self.assertEqual(math.ceil(14.5), positive_num)

    def test_ceil_method_positive_number_with_decimal_lower_to_point_5(self):
        positive_num = 9
        self.assertEqual(math.ceil(8.4), positive_num)

    def test_ceil_method_positive_number_with_decimal_equals_to_point_0(self):
        positive_num = 4
        self.assertEqual(math.ceil(4.0), positive_num)

    def test_ceil_method_negative_number_with_decimal_greater_than_point_5(self):
        negative_num = -10
        self.assertEqual(math.ceil(-10.6), negative_num)

    def test_ceil_method_negative_number_with_decimal_equals_to_5(self):
        negative_num = -14
        self.assertEqual(math.ceil(-14.5), negative_num)

    def test_ceil_method_negative_number_with_decimal_lower_to_5(self):
        negative_num = -8
        self.assertEqual(math.ceil(-8.4), negative_num)

    def test_ceil_method_negative_number_with_decimal_equals_to_point_0(self):
        positive_num = -4
        self.assertEqual(math.ceil(-4.0), positive_num)

    def test_ceil_method_positive_big_number_with_decimal_greater_than_5(self):
        positive_num = 43536543465
        self.assertEqual(math.ceil(43536543464.7), positive_num)

    def test_ceil_method_string_value(self):
        cad = "number"
        self.assertRaises(TypeError,math.ceil,cad)


if __name__ == '__main__':
    unittest.main()