import math
import unittest


class TestMathPowFunction(unittest.TestCase):

    def test_pow_method_positive_number(self):
        positive_num = 4
        self.assertEqual(math.pow(2, 2), positive_num)

    def test_pow_method_big_positive_number(self):
        positive_num = 3.5997658189006033e+22
        self.assertEqual(math.pow(32453, 5), positive_num)

    def test_pow_method_negative_number(self):
        negative_num = 16.0
        self.assertEqual(math.pow(-4,2), negative_num)

    def test_pow_method_string(self):
        cad = "number"
        self.assertRaises(TypeError, math.pow, (cad, 2))


if __name__ == "__name__":
    unittest.main()