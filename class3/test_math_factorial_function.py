import math
import unittest


class TestMathFactorialFunction(unittest.TestCase):

    def test_factorial_method_positive_number_1(self):
        positive_num = 1
        self.assertEqual(math.factorial(1), 1)

    def test_factorial_method_positive_number(self):
        positive_num = 3628800
        self.assertEqual(math.factorial(10), positive_num)

    def test_factorial_method_big_positive_number(self):
        positive_num = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
        self.assertEqual(math.factorial(100), positive_num)

    def test_factorial_method_string(self):
        cad = 'cad'
        self.assertRaises(TypeError,math.factorial,cad)

if __name__ == '__main__':
    unittest.main()