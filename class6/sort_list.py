"""@package SortList
Class that will read a CSV file (unsorted values) and will return a sorted CSV file.
"""
import csv
from datetime import datetime
import utils


class SortLists:
    """SortList class have two methods to sort data, MergeSort and Quicksort. It also provides
    a method to evaluate the performance of each method. The class has one method to read
    the data from a csv and one for saving the ordered list in a csv file.
    """

    def __init__(self):
        """Constructor"""

        ## Keeps the unordered list. After running any sorting method will save the ordered list
        self.list_of_nums = []
        ## Keeps a copy of the original list as reference. It is refreshed in each iteration to have the latest data
        self.reference_list = []
        ## Keeps the max numbers saved in the output file per line
        self.num_of_elems_per_line = 5
        ## Used for performance analysis, keeps the start time (in seconds) of the sorting method
        self.start_time = 0
        ##  Used for performance analysis, keeps the end time (in seconds)  of the sorting method
        self.end_time = 0
        ## Keeps the number of secods that the sorting method took
        self.time_consumed = 0

    def set_input_data(self, file_name_path):
        """Method used to load the data read from a csv file into a list

        Args:

            file_name_path (str): Tne path of the source file (csv)

        Raises:

            ValueError: Error  catched and an error code is returned (-2) instead
            FileNotFoumdError: Error catched and an error code is returned (-1) instead

        Returns:

            list: If file opened correctly. method returns the list with the data read from the csv file
        """

        try:
            self.list_of_nums = []
            with open(file_name_path) as input_file:
                reader = csv.reader(input_file)
                for row in reader:
                    for elem in row:
                        self.list_of_nums.append(int(elem))
                return self.list_of_nums
        except ValueError:
            # raise NotANumberError("Value read is not a number")
            print("Value read is not a number")
            return -2
        except FileNotFoundError:
            # raise FileNameIsNotCorrectError("Error while trying to open the file")
            print("Error while trying to open the file")
            return -3

    def set_output_data(self, file_name_path):
        """Method used to save the ordered list into a csv file

        Args:

            file_name_path (str): Tne path of the source file (csv)

        Raises:

            FileNotFoumdError: Error catched and an error code is returned (-3) instead

        Returns:

             int: return 1 if the file got saved correctly
        """
        try:
            with open(file_name_path, "w", newline='') as output_file:
                writer = csv.writer(output_file)
                length_of_list = len(self.list_of_nums)
                # print(length_of_list)
                bottom_index = 0
                num_of_elems_per_line = self.num_of_elems_per_line
                starting_index = self.num_of_elems_per_line
                i = 0
                for i in range(starting_index, length_of_list + 1, num_of_elems_per_line):
                    writer.writerow(self.list_of_nums[bottom_index: i])
                    bottom_index = i
                else:
                    # To add the remaining elements when length_of_list is not divisible
                    # equally by length_of_elements_per_line
                    if i < length_of_list:
                        writer.writerow(self.list_of_nums[i:])
                return 1
        except FileNotFoundError:
            # raise FileNameIsNotCorrectError("Error while trying to open the file")
            print("Error while trying to open the file")
            return -3

    def set_list_value(self, u_list):
        """Method used to directly assign the list_of_numbers for testing purpose

        Args:
            u_list (list): list that represents the unordered list
        """
        self.list_of_nums = u_list

    def execute_quick_sort(self):
        """ Method called to use the QuickSort algorithm using the variable list_of_number as the source of the data

        Returns:

             list: List ordered, if input list is not empty
        """
        self.start_time = datetime.now()
        if self.list_of_nums:
            l_index = 0
            r_index = len(self.list_of_nums) - 1
            self.__quick_sort__(l_index, r_index)
            self.end_time = datetime.now()
            return self.list_of_nums

        self.end_time = datetime.now()
        return []

    def __quick_sort__(self, low_index, high_index):
        """Method that execute the logic for QuickSort

        Args:

            low_index (int): Represents the left starting index of the list
            high_index (int): Represents the right ending index of the list
        """
        pivot = high_index
        pivot_value = self.list_of_nums[pivot]
        right = high_index - 1
        left = low_index

        left_greater = False
        right_lower = False

        if low_index < high_index:
            while left <= right:

                l_value = self.list_of_nums[left]
                r_value = self.list_of_nums[right]

                if l_value > pivot_value:
                    left_greater = True
                else:
                    left += 1

                if r_value < pivot_value:
                    right_lower = True
                else:
                    right -= 1

                if left_greater and right_lower:
                    aux = self.list_of_nums[left]
                    self.list_of_nums[left] = self.list_of_nums[right]
                    self.list_of_nums[right] = aux
                    left_greater = False
                    right_lower = False
                    left += 1
                    right -= 1

            aux = self.list_of_nums[left]
            self.list_of_nums[left] = self.list_of_nums[pivot]
            self.list_of_nums[pivot] = aux
            pivot = left
            self.__quick_sort__(low_index, pivot - 1)
            self.__quick_sort__(pivot + 1, high_index)

    def execute_merge_sort(self):
        """Method that execute the logic for MergeSort

        Returns:

            list: List ordered, if input list is not empty
        """
        self.start_time = datetime.now()
        l_index = 0
        r_index = len(self.list_of_nums) - 1
        self.__merge_sort__(l_index, r_index)
        self.end_time = datetime.now()
        return self.list_of_nums

    def __merge_sort__(self, l_index, r_index):
        """Method that starts the logic for MergeSort algorithm

        Args:

            l_index: Represents the left starting index of the list
            r_index: Represents the right starting index of the list
        """
        if l_index < r_index:
            halve_index = l_index + (r_index - l_index) // 2
            self.__merge_sort__(l_index, halve_index)
            self.__merge_sort__(halve_index + 1, r_index)
            self.__merge__(l_index, halve_index, r_index)

    def __merge__(self, l_list, halve_index, r_list):
        """Method that do the merge in a specific range in the list

        Args:
            l_list: Represents the left starting index of the list
            halve_index: works as a pivot
            r_list: Represents the right starting index of the list
        """

        self.reference_list = self.list_of_nums[:]
        first_idx = l_list
        second_idx = halve_index + 1

        for i in range(l_list, r_list + 1):

            if first_idx > halve_index:
                break

            elif second_idx > r_list:
                 self.list_of_nums[i] = self.reference_list[first_idx]
                 first_idx += 1

            else:
                first_num = self.reference_list[first_idx]
                second_num = self.reference_list[second_idx]

                if first_num > second_num:
                    self.list_of_nums[i] = second_num
                    second_idx += 1
                else:
                    self.list_of_nums[i] = first_num
                    first_idx += 1

    def get_performance_data(self):
        """Method that provides some information about the performance of the last execution of a sorting method

        Returns:

             dictionary: {size: how many elemets were used to sort
                                time_in_sec: The amount of time in seconds that the sorting method taok to execute
                                start_time: The time at which the sorting method started the execution algorithm
                                end_time: The time at which the sorting method completed the execution of the algorithm
                         }
        """
        self.time_consumed = utils.get_time_elapsed_in_seconds(self.start_time, self.end_time)
        return {"size": len(self.list_of_nums),
                "time_in_sec": self.time_consumed,
                "start_time": str(self.start_time),
                "end_time": str(self.end_time)}

srt_list = SortLists()

list_returned =srt_list.set_input_data("./data_files/nums.csv")
print(list_returned)
srt_list.execute_merge_sort()
# srt_list.execute_quick_sort()
# srt_list.set_output_data("./data_files/nums_quick_ordered.csv")
# print(srt_list.get_performance_data())
