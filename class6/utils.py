"""Module used to calculate time elapsed in seconds"""

def get_time_in_seconds(date_time):
    """Method used to calculate the time in secods

    Args:

        date_time: date in datatime format

    Returns:

        int: time in secods
    """
    start_time = str(date_time.strftime("%X"))
    (h, m, s) = start_time.split(':')
    time_in_seconds = int(h) * 3600 + int(m) * 60 + int(s)
    return int(time_in_seconds)


def get_time_elapsed_in_seconds(start_time, end_time):
    """Method used to calculate the time that has passed between two datetimes

    Args:

        start_time: datetime for starting time
        end_time: datetime for the ending time

    Returns:

        int: The difference in secods between the two date times
    """
    start_time_in_seconds = get_time_in_seconds(start_time)
    end_time_in_seconds = get_time_in_seconds(end_time)
    return end_time_in_seconds - start_time_in_seconds
