from mypowerlist import MyPowerList

my_list = MyPowerList()

my_list.add_item(10)
my_list.add_item(12)

my_list.add_item(7)
my_list.add_item(10)
my_list.add_item(2)
my_list.add_item(120)
my_list.add_item(12)
my_list.add_item(100)
my_list.add_item(20)

my_list.save_to_file("archivo.txt")
list_from_file = MyPowerList.read_from_file("new_list_data.txt")

print(f"List Data from File: {list_from_file.lista}")