from mypowerlist import MyPowerList

my_list = MyPowerList()

my_list.add_item(0)
my_list.add_item(1)
my_list.add_item(13)
my_list.add_item(45)
my_list.add_item(5)
my_list.add_item(9)
my_list.add_item(4)
my_list.add_item(0)

print("Showing the original list: ", my_list.lista)
print(f"Sorting List: ", my_list.sort_list())
