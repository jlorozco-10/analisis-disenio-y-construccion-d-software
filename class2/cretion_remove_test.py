from mypowerlist import MyPowerList


my_list = MyPowerList()

my_list.add_item(1)
my_list.add_item(3)
my_list.add_item(7)
my_list.add_item(4)
my_list.add_item(10)
my_list.add_item(6)
my_list.add_item(20)
my_list.add_item(0)
my_list.add_item(2)
my_list.add_item(3)
my_list.add_item(9)

print("Showing the original list: ", my_list.lista)
index = 8

my_list.remove_nth_elemet(index)
print(f'List after removing element in index {index}: ', my_list.lista)
