import json
from pathlib import Path


class Contacts:

    def __init__(self):
        self.directory = []

    def add_contact(self, name, address, phone, email):
        contact = {"name": name, "address": address, "phone": phone, "email": email}
        self.directory.append(contact)

    def save_all_contacts(self,filename):
        data = json.dumps(self.directory)
        path = Path(filename).write_text(data)

    def load_records_from_file(self, file_name):
        data = Path(file_name).read_text()
        conctact_list = json.loads(data)
        self.directory += conctact_list

    def search_by_name(self, name):
        return [dic for dic in self.directory if dic['name'] == name]

