class MyPowerList:

    def __init__(self, *data):
        if len(data)>0:
            self.lista = list(data)[0]
        else:
            self.lista =[]

    def add_item(self, elem):
        self.lista.append(elem)

    def remove_nth_elemet(self, idx):
        tam = len(self.lista)

        if idx < tam:
            self.lista.pop(idx)
        else:
            print("Error: Index not present in List")

    def sort_list(self):
        sorted_list = self.lista
        tam = len(self.lista)

        for idx in range(1, tam):
            elem_to_sort = sorted_list[idx]
            for idx2 in range(idx - 1, -1, -1):
                elem_to_evaluate = sorted_list[idx2]

                if elem_to_sort < elem_to_evaluate:
                    sorted_list[idx2 + 1] = elem_to_evaluate
                    sorted_list[idx2] = elem_to_sort
                else:
                    break
        return sorted_list

    def l_merge(self, list_to_merge):
        self.lista = list_to_merge + self.lista

    def r_merge(self, list_to_merge):
        self.lista = self.lista + list_to_merge

    def save_to_file(self, file_name):
        file = open(file_name, "w")
        for elem in self.lista:
            file.writelines(str(elem)+"\n")
        print(f"{file_name} saved correctly ")

    @classmethod
    def read_from_file(cls, file_name):
        print(f"Getting data from {file_name}")
        file = open(file_name, "r")
        new_list = []
        for elem in file:
            new_list.append(int(elem))

        return cls(new_list)


