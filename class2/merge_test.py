from mypowerlist import MyPowerList


my_list = MyPowerList()

my_list.add_item(3)
my_list.add_item(4)
my_list.add_item(8)
my_list.add_item(1)
my_list.add_item(5)

list2 = [10,20,30,40]

print(f"Base list: {my_list.lista}")
print(f"List to merge: {list2}")
my_list.l_merge(list2)
print(f"my_list after l_merge: {my_list.lista}")

my_list2 = MyPowerList()

my_list2.add_item(4)
my_list2.add_item(8)
my_list2.add_item(2)
my_list2.add_item(10)
my_list2.add_item(6)

print(f"\nBase list: {my_list2.lista}")
print(f"List to merge: {list2}")
my_list2.r_merge(list2)
print(f"my_list2 after r_merge: {my_list2.lista}")
