from contacts import Contacts
from pathlib import Path
import json

contacts = Contacts()
contacts.add_contact("Luis","Av. Acueducto #1234","333-323-4332","luis@test.com")
contacts.add_contact("Dany","Av. Pedro Moreno #1111","323-345-7652","pedro@test.com")
contacts.add_contact("Lorena","Av. Naciones Unidas #233","332-221-4562","lorena@test.com")
contacts.add_contact("Mike","Av. Patria #233","386-781-9789","miguel@test.com")

print("Creating a directory of contacts and saving it to a file")
contacts.save_all_contacts("contacts.json")

contacts2 = Contacts()
contacts2.add_contact("Lucas", "Av. Naciones Unidas #233", "386-781-9789", "lucas@test.com")
contacts2.add_contact("Lisa", "Av. Vallarta #233", "386-781-9789", "lisa@test.com")

contacts2.load_records_from_file("contacts.json")
print("\nLoading records from a file")
print(contacts2.directory)


contact_name = "Dany"
print(f'\nSearching for element ({contact_name}) in list: {contacts2.search_by_name(contact_name)}')