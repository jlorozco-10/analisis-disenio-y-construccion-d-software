import json
import logging

from pathlib import Path

# logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Contacts:

    def __init__(self):
        self.directory = []


    def add_contact(self, name, address, phone, email):
        logger.info("adding a contact to the list")
        contact = {"name": name, "address": address, "phone": phone, "email": email}
        self.directory.append(contact)
        logger.info("contact added correctly with data: %s"%contact)


    def save_all_contacts(self,filename):
        try:
            logger.info("saving the directory list to file %s" % filename)
            data = json.dumps(self.directory)
            path = Path(filename).write_text(data)
            logger.info("file saved correctly")
        except FileNotFoundError:
            logger.error("No such file or directory: %s",filename)


    def load_records_from_file(self, file_name):
        try:
            logger.info("Reading file %s and adding the data to the directory" % file_name)
            data = Path(file_name).read_text()
            conctact_list = json.loads(data)
            self.directory += conctact_list
            logger.info("data read from file correctly")
        except FileNotFoundError:
            logger.error("File %s not found",file_name)


    def search_by_name(self, name):
        logger.info("Searching contact with name %s" % name)
        contact = [dic for dic in self.directory if dic['name'] == name]
        if len(contact) > 0:
            logger.info("data found: %s"% contact)
            return contact
        else:
            logger.error("Contact with name %s not found", name)
            return -1


