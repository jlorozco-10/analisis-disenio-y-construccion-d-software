import unittest
import os.path
import logging
from ddt import ddt, file_data
from sort_list import SortLists

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("sort_list")


fh = logging.FileHandler('sort_list.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

@ddt
class TestSort(unittest.TestCase):

    def test_sort_list_with_empty_list(self):
        s = SortLists()
        expected_result = []
        s.set_list_value([])
        self.assertListEqual(s.execute_merge_sort(), expected_result)

    def test_set_input_data_existing_file(self):
        s = SortLists()
        data = [1,4,3,2,10,5]
        self.assertListEqual(s.set_input_data("./data_files/data.csv"), data)

    def test_set_input_data_not_existing_file(self):
        s = SortLists()
        error_code = -3  # -3 "Error while trying to open the file"
        self.assertEqual(s.set_input_data("./data_files/file_no_existing.csv"), error_code)

    def test_set_input_data_list_with_no_numeric_characters(self):
        s = SortLists()
        error_code = -2  # -2 "Value read is not a number"
        self.assertEqual(s.set_input_data("./data_files/data_with_characters.csv"), error_code)

    def test_set_input_data_empty_file(self):
        s = SortLists()
        data = []  # -2 "Value read is not a number"
        self.assertListEqual(s.set_input_data("./data_files/empty_file.csv"), [])

    def test_execute_merge_sort_ordered_list_with_two_numbers(self):
        # 10,5,2,0,39
        s = SortLists()
        expected_result = [10,15]
        s.set_input_data("./data_files/ordered_list_with_two_numbers.csv")
        self.assertListEqual(s.execute_merge_sort(), expected_result)

    def test_execute_merge_sort_unordered_list_with_two_numbers(self):
        # 10,5,2,0,39
        s = SortLists()
        expected_result = [10,15]
        s.set_input_data("./data_files/ordered_list_with_two_numbers.csv")
        self.assertListEqual(s.execute_merge_sort(), expected_result)

    def test_execute_merge_sort_short_list(self):
        # 10,5,2,0,39
        s = SortLists()
        expected_result = [0, 2, 5, 10, 39]
        s.set_input_data("./data_files/nums.csv")
        self.assertListEqual(s.execute_merge_sort(), expected_result)

    def test_set_output_data_check_cretion_of_output_file(self):
        s = SortLists()
        data = [1, 2, 3, 4, 5, 10]
        input_file_path= "./data_files/data.csv"
        output_file_path="./data_files/data_output.csv"
        s.set_input_data(input_file_path)
        s.execute_merge_sort()
        saved_file_status = s.set_output_data(output_file_path)
        self.assertEqual(saved_file_status,1)
        self.assertTrue(os.path.isfile(output_file_path))

    def test_set_output_data_validating_data_in_output_file(self):
        s = SortLists()
        expected_data = [1, 2, 3, 4, 5, 10]
        input_file_path = "./data_files/data.csv"
        output_file_path = "./data_files/data_output.csv"
        s.set_input_data(input_file_path)
        s.execute_merge_sort()
        saved_file_status = s.set_output_data(output_file_path)
        self.assertEqual(saved_file_status, 1)

        # Validing if data saved in the output file is correct
        list_saved = s.set_input_data(output_file_path)
        self.assertListEqual(list_saved,expected_data)

    @file_data('test_data.json')
    def test_execute_merge_sort_ddt_multiple_tests(self, unordered_list, ordered_list):
        # print(unordered_list)
        # print(ordered_list)
        s = SortLists()
        s.set_list_value(unordered_list)
        ordered_list_returned = s.execute_merge_sort()
        self.assertListEqual(ordered_list_returned, ordered_list)

    # @file_data('test_data_one_million_numbers.json')
    # def test_execute_merge_sort_ddt_one_million_numbers(self, unordered_list, ordered_list):
    #     # print(unordered_list)
    #     # print(ordered_list)
    #     s = SortLists()
    #     s.set_list_value(unordered_list)
    #     ordered_list_returned = s.execute_merge_sort()
    #     self.assertListEqual(ordered_list_returned, ordered_list)


if __name__ == "__main__":
    unittest.main()

